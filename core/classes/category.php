<?php
/**
 * Created by PhpStorm.
 * User: tn116
 * Date: 14-09-2017
 * Time: 09:32
 */

class category
{
    public $db;
    public $iCatID;
    public $vcName;
    public $txDescription;
    public $iDeleted;

    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iCatID" => "ID",
            "vcName" => "Category Name",
            "txDescription" => "Description"

        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iCatID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "vcName" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "txDescription" => array("textarea", FILTER_SANITIZE_STRING, TRUE, ""),


        );

        $this->arrValues = array();
    }

    /**
     * function to get list of categories
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM category WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }


    /**
     * function to get a single category
     * @param $iCatID
     */
    public function getItem($iCatID) { //set parameter iCatID to get a single category
        $this->iCatID = $iCatID;
        $sql = "SELECT * FROM category WHERE iCatID = ? AND iDeleted = 0";
        $row = $this->db->_fetch_array($sql, array($this->iCatID));
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }
        //showme($row);
    }


    public function getAllCategories($Limit) {
        $sql = "SELECT * FROM category WHERE iDeleted = 0 LIMIT " . $Limit;
        return $this->db->_fetch_array($sql);

    }


    /**
     * @return int
     *
     */
    public function save() {
        if ($this->iCatID > 0) {
            //UPDATE MODE
            $params = array(
                $this->vcName,
                $this->txDescription,
                $this->iCatID,
            );

            $sql = "UPDATE category SET " .
                "txDescription = ?, " .
                "vcName = ? " .
                "WHERE iCatID = ? ";

            $this->db->_query($sql, $params);
            return $this->iCatID;


        } else {
            //CREATE MODE
            $params = array(
                $this->vcName,
                $this->txDescription,

            );

            $sql = "INSERT INTO category (" .
                "vcName, " .
                "txDescription) " .
                "VALUES(?,?)";
            //exit();
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    public function delete() {
        $params = array($this->iCatID);

        $sql = "UPDATE category set " .
            "iDeleted = 1 " .
            "WHERE iCatID = ? ";
        $this->db->_query($sql, $params);

    }

}

