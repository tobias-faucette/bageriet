<?php

class product
{
    public $db;
    public $iProductID;
    public $iCatID;
    public $vcTitle;
    public $iLikes;
    public $vcImage;
    public $daCreated;
    public $txDesc;
    public $iIsActive;
    public $iDeleted;

    public $arrLabels;
    public $arrFormElms;
    public $arrValues;
    public $arrGroups;


    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iProductID" => "ID",
            "iCatID" => "Category",
            "iLikes" => "Likes",
            "vcTitle" => "Product Name",
            "vcImage" => "Image",
            "daCreated" => "Created",
            "txDesc" => "Short Description",
            "iIsActive" => "Active",

        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iProductID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "iCatID" => array("select", FILTER_VALIDATE_INT, FALSE, 0),
            "iLikes" => array("text", FILTER_VALIDATE_INT, FALSE, 0),
            "vcTitle" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcImage" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "daCreated" => array("hidden", FILTER_VALIDATE_INT, FALSE, ""),
            "txDesc" => array("textEdit", FILTER_SANITIZE_STRING, TRUE, ""),
            "iIsActive" => array("number", FILTER_VALIDATE_INT, FALSE, ""),


        );

        $this->arrValues = array();
    }


    /**
     * function to get list of product
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM product WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }

    /**
     * @param $Limit
     * @return array
     */

    public function getAllProduct($Limit) {
        $sql = "SELECT * FROM product WHERE iDeleted = 0 ORDER BY RAND() ASC, daCreated DESC LIMIT " . $Limit;
        return $this->db->_fetch_array($sql);

    }

    /**
     * @param $Limit
     * @return array
     */
    public function getPopulareproduct($Limit) {
        $sql = "SELECT vcTitle FROM product WHERE iDeleted = 0 ORDER BY iLikes DESC LIMIT " . $Limit;
        return $this->db->_fetch_array($sql);

    }




    /**
     * function to get a single Event
     * @param $iProductID
     * @return array
     */
//    public function getProduct($iProductID) { //set parameter iProductID to get a single Event
//        $this->iProductID = $iProductID;
//        $sql = "SELECT * FROM product WHERE iProductID = ? AND iDeleted = 0";
//        $row = $this->db->_fetch_array($sql, array($this->iProductID));
//        foreach ($row[0] as $key => $value) {
//            $this->$key = $value;
//        }
//
//        return $row;
//        //showme($row);
//    }


    public function getProduct($iProductID) {
        $this->iProductID = $iProductID;
        $sql = "SELECT p.*, c.iCatID, c.vcName " .
            "FROM product p " .
            "LEFT JOIN category c " .
            "ON p.iCatID = c.iCatID " .
            "WHERE iProductID = ? " .
            "AND p.iDeleted = 0";
        if ($row = $this->db->_fetch_array($sql, array($this->iProductID))) {
            foreach ($row[0] as $key => $value) {
                $this->$key = $value;
            }
            $this->arrGroups = $this->getgrouprelations();

            foreach ($this->arrGroups as $value) {
                $role = strtolower($value["vcTitle"]);
                $this->$role = 1;
            }
        }
    }

    /**
     * Get product Ingredients using rel table
     * @param $iProductID
     * @return array
     */

    public function getIngredientRel($iProductID) {
        $this->iProductID = $iProductID;
        $sql = "SELECT r.*, i.iIngredientsID, i.vcTitle " .
                "FROM product_ingredients r " .
                "LEFT JOIN ingredients i " .
                "ON r.iIngredientsID = i.iIngredientsID " .
                "WHERE r.iProductID = $iProductID";

        return $this->db->_fetch_array($sql);


    }

    /**
     * Save item
     */
    public function save() {
        if ($this->iProductID > 0) {
            //UPDATE MODE
            $params = array(
                $this->iCatID,
                $this->iLikes,
                $this->vcTitle,
                $this->vcImage,
                $this->daCreated,
                $this->txDesc,
                $this->iIsActive,
                $this->iProductID
            );

            $sql = "UPDATE product SET " .
                "iCatID = ?, " .
                "iLikes = ?, " .
                "vcTitle = ?, " .
                "vcImage = ?, " .
                "daCreated = ?, " .
                "txDesc = ?, " .
                "iIsActive = ? " .
                "WHERE iProductID = ? ";

            $this->db->_query($sql, $params);
            return $this->iProductID;

        } else {
            //CREATE MODE
            $params = array(
                $this->iCatID,
                $this->iLikes,
                $this->vcTitle,
                $this->vcImage,
                time(),
                $this->txDesc,
                $this->iIsActive
            );

            $sql = "INSERT INTO product (" .
                "iCatID, " .
                "iLikes, " .
                "vcTitle, " .
                "vcImage, " .
                "daCreated, " .
                "txDesc, " .
                "iIsActive) " .
                "VALUES(?,?,?,?,?,?,?)";
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    /**
     * Delete item
     */

    public function delete() {
        $params = array($this->iProductID);

        $sql = "UPDATE product SET " .
            "iDeleted = 1 " .
            "WHERE iProductID = ? ";
        $this->db->_query($sql, $params);
    }


    /**
     * Get user related ingredients
     * @return array of related ingredients
     */
    public function getgrouprelations() {
        $params = array($this->iProductID);
        $strSelect = "SELECT i.iIngredientsID, i.vcTitle, x.vcAmount " .
            "FROM ingredients i  " .
            "LEFT JOIN product_ingredients x " .
            "ON x.iIngredientsID = i.iIngredientsID " .
            "WHERE x.iProductID = ? " .
            "AND i.iDeleted = 0";
        return $this->db->_fetch_array($strSelect, $params);
    }


    public function getCategory() {
        $sql = "SELECT p.iCatID, c.iCatID, c.vcName " .
            "FROM product p " .
            "LEFT JOIN category c " .
            "ON p.iCatID = c.iCatID " .
            "WHERE p.iDeleted = 0";
        return $this->db->_fetch_array($sql);
    }
}