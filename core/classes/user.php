<?php

/**
 * Created City PhpStorm.
 * User: tn116
 * Date: 17-05-2017
 * Time: 09:21
 */
class user
{
    public $db;
    public $iUserID;
    public $vcUserName;
    public $vcPassword;
    public $vcFirstName;
    public $vcLastName;
    public $vcAddress;
    public $iZip;
    public $vcCity;
    public $vcEmail;
    public $vcPhone1;
    public $vcPhone2;
    public $vcImage;
    public $iOrgID;
    public $daCreated;
    public $iSuspended;
    public $iDeleted;

    public $arrLabels;
    public $arrFormElms;
    public $arrValues;
    public $arrGroups;

    public $sysadmin;
    public $admin;
    public $extranet;
    public $newsletter;

    public function __construct() {
        global $db;
        $this->db = $db;

        $this->arrLabels = array(
            "iUserID" => "ID",
            "vcUserName" => "Username",
            "vcPassword" => "Password",
            "vcFirstName" => "First Name",
            "vcLastName" => "Last Name",
            "vcAddress" => "Address",
            "iZip" => "zip code",
            "vcCity" => "City",
            "vcEmail" => "Email",
            "vcPhone1" => "phone",
            "vcPhone2" => "Mobile",
            "vcImage" => "Image",
            "iOrgID" => "Organization",
            "daCreated" => "Created",
            "iSuspended" => "Suspended"


        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iUserID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "vcUserName" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcPassword" => array("password", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcFirstName" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcLastName" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcAddress" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "iZip" => array("number", FILTER_VALIDATE_INT, FALSE, ""),
            "vcCity" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "vcEmail" => array("email", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcPhone1" => array("number", FILTER_VALIDATE_INT, FALSE, ""),
            "vcPhone2" => array("number", FILTER_VALIDATE_INT, FALSE, ""),
            "vcImage" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "iOrgID" => array("select", FILTER_VALIDATE_INT, FALSE, ""),
            "daCreated" => array("hidden", FILTER_VALIDATE_INT, FALSE, ""),
            "iSuspended" => array("number", FILTER_VALIDATE_INT, FALSE, "")

        );

        $this->arrValues = array();

    }

    /**
     * function to get list of users
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM user WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }


    /**
     * @param int $iUserID
     * Selects City id and add values to class properties
     * Used City Auth initUser to get group relations on login
     */
    public function getuser($iUserID) {
        $this->iUserID = $iUserID;
        $sql = "SELECT u.*, o.vcOrgName " .
            "FROM user u " .
            "LEFT JOIN org o " .
            "ON u.iOrgID = o.iOrgID " .
            "WHERE iUserID = ? " .
            "AND u.iDeleted = 0";
        if ($row = $this->db->_fetch_array($sql, array($this->iUserID))) {
            foreach ($row[0] as $key => $value) {
                $this->$key = $value;
            }
            $this->arrGroups = $this->getgrouprelations();

            foreach ($this->arrGroups as $value) {
                $role = strtolower($value["vcRoleName"]);
                $this->$role = 1;
            }
        }
    }


    /**
     * @return int
     *
     */
    public function save() {
        if ($this->iUserID > 0) {
            //UPDATE MODE
            $params = array(
                $this->vcUserName,
                $this->hashPassword($this->vcPassword),
                $this->vcFirstName,
                $this->vcLastName,
                $this->vcAddress,
                $this->iZip,
                $this->vcCity,
                $this->vcEmail,
                $this->vcPhone1,
                $this->vcPhone2,
                $this->vcImage,
                $this->iOrgID,
                $this->daCreated,
                $this->iSuspended,
                $this->iUserID
            );

            $sql = "UPDATE user SET " .
                "vcUserName = ?, " .
                "vcPassword = ?, " .
                "vcFirstName = ?, " .
                "vcLastName = ?, " .
                "vcAddress = ?, " .
                "iZip = ?, " .
                "vcCity = ?, " .
                "vcEmail = ?, " .
                "vcPhone1 = ?, " .
                "vcPhone2 = ?, " .
                "vcImage = ?, " .
                "iOrgID = ?, " .
                "daCreated = ?, " .
                "iSuspended = ? " .
                "WHERE iUserID = ? ";

            $this->db->_query($sql, $params);
            return $this->iUserID;
        } else {
            //CREATE MODE
            $params = array(
                $this->vcUserName,
                $this->hashPassword($this->vcPassword),
                $this->vcFirstName,
                $this->vcLastName,
                $this->vcAddress,
                $this->iZip,
                $this->vcCity,
                $this->vcEmail,
                $this->vcPhone1,
                $this->vcPhone2,
                $this->vcImage,
                $this->iOrgID,
                time(),
                $this->iSuspended
            );

            echo $sql = "INSERT INTO user (" .
                "vcUserName, " .
                "vcPassword, " .
                "vcFirstName, " .
                "vcLastName, " .
                "vcAddress, " .
                "iZip, " .
                "vcCity, " .
                "vcEmail, " .
                "vcPhone1, " .
                "vcPhone2, " .
                "vcImage, " .
                "iOrgID, " .
                "daCreated, " .
                "iSuspended) " .
                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    /**
     * Get user related groups
     * @return array of related groups
     */
    public function getgrouprelations() {
        $params = array($this->iUserID);
        $strSelect = "SELECT g.iGroupID, g.vcGroupName, g.vcRoleName " .
            "FROM usergroup g " .
            "LEFT JOIN usergrouprel x " .
            "ON x.iGroupID = g.iGroupID " .
            "WHERE x.iUserID = ? " .
            "AND g.iDeleted = 0";
        return $this->db->_fetch_array($strSelect, $params);
    }


    public function delete() {
        $params = array($this->iUserID);

        $sql = "UPDATE user SET " .
            "iDeleted = 1 " .
            "WHERE iUserID = ? ";
        $this->db->_query($sql, $params);

    }

    /**
     * Return a securely salted SHA256 hash of the entered password
     *
     * @param string $password
     *
     * @return string
     */
    public static function hashPassword($password) {
        $salt = "!k8B</)%H;#:%DUS(Y)#A{l^H{u%F@V<;z@-Gc5C/[;`y7'm%9RH4RP(K^NDQYrh"; // Generate your own salt keys! Do not use this one!
        for ($i = 0; $i < 10; $i++) {
            $password = str_rot13(strrev(hash("sha256", $password))) . $salt;
        }

        $password = hash("sha256", str_rot13(strrev(strrev($password) . $salt)));

        return $password;
    }

}

