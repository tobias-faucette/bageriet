<?php
/**
 * Created by PhpStorm.
 * User: tn116
 * Date: 31-08-2017
 * Time: 22:13
 */

class slider
{
    public $db;
    public $iSlideID;
    public $vcTitle;
    public $vcImage;
    public $vcCaption;
    public $vcTag;
    public $iIsActive;
    public $iDeleted;

    public $arrLabels;
    public $arrFormElms;
    public $arrValues;

    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iSlideID" => "ID",
            "vcTitle" => "Image Title",
            "vcImage" => "Image URL",
            "vcCaption" => "Image Caption",
            "vcTag" => "Alt tag",
            "iIsActive" => "Active",
        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iSlideID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "vcTitle" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcImage" => array("text", FILTER_SANITIZE_STRING, FALSE, "0"),
            "vcCaption" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "vcTag" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "iIsActive" => array("number", FILTER_VALIDATE_INT, FALSE, 0),



        );

        $this->arrValues = array();
    }


    /**
     * function to get list of image_slider
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM image_slider WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }


    /**
     * function to get a single record
     * @param $iSlideID
     * @return array
     */
    public function getImage_slider($iSlideID) { //set parameter iSlideID to get a single record
        $this->iSlideID = $iSlideID;
        $sql = "SELECT * FROM image_slider WHERE iSlideID = ? AND iDeleted = 0";
        $row = $this->db->_fetch_array($sql, array($this->iSlideID));
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }

        return $row;
        //showme($row);
    }


    public function sliderCounter(){
        $sql = "SELECT COUNT(*) FROM image_slider WHERE iDeleted = 0";

        $row = $this->db->_fetch_array($sql);
        return $row[0];
    }


    /**
     * Save item
     */
    public
    function save() {
        if ($this->iSlideID > 0) {
            //UPDATE MODE
            $params = array(
                $this->vcTitle,
                $this->vcImage,
                $this->vcCaption,
                $this->vcTag,
                $this->iIsActive

            );

            $sql = "UPDATE image_slider SET " .
                "vcTitle = ?, " .
                "vcImage = ?, " .
                "vcCaption = ?, " .
                "vcTag = ?, " .
                "iIsActive = ? " .
                "WHERE iSlideID = ? ";

            $this->db->_query($sql, $params);
            return $this->iSlideID;

        } else {
            //CREATE MODE
            $params = array(
                $this->vcTitle,
                $this->vcImage,
                $this->vcCaption,
                $this->vcTag,
                $this->iIsActive
            );

            $sql = "INSERT INTO image_slider (" .
                "vcTitle, " .
                "vcImage, " .
                "vcCaption, " .
                "vcTag, " .
                "iIsActive) " .
                "VALUES(?,?,?,?,?)";
            //exit();
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    /**
     * Delete item
     */

    public
    function delete() {
        $params = array($this->iSlideID);

        $sql = "UPDATE image_slider SET " .
            "iDeleted = 1 " .
            "WHERE iSlideID = ? ";
        $this->db->_query($sql, $params);
    }
}