<?php

/**
 * Created City PhpStorm.
 * org: tn116
 * Date: 17-05-2017
 * Time: 09:21
 */
class org
{
    public $db;
    public $iOrgID;
    public $vcOrgName;
    public $vcAddress;
    public $iZip;
    public $vcCity;
    public $vcEmail;
    public $vcPhone;
    public $vcFax;
    public $vcCountry;
    public $daCreated;
    public $iDeleted;


    public $arrLabels;
    public $arrFormElms;
    public $arrValues;

    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iOrgID" => "ID",
            "vcOrgName" => "Firm Name",
            "vcAddress" => "Adresse",
            "iZip" => "Zip Code",
            "vcCity" => "City",
            "vcEmail" => "Email",
            "vcPhone" => "Phone Number",
            "vcFax" => "fax number",
            "vcCountry" => "Country",
            "daCreated" => "Created",


        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iOrgID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "vcOrgName" => array("text", FILTER_SANITIZE_STRING, TRUE, "hej"),
            "vcAddress" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "iZip" => array("number", FILTER_VALIDATE_INT, TRUE, ""),
            "vcCity" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcEmail" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcPhone" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "vcFax" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "vcCountry" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "daCreated" => array("hidden", FILTER_SANITIZE_STRING, FALSE, ""),


        );

        $this->arrValues = array();
    }

    /**
     * function to get list of orgsR
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM org WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }


    /**
     * function to get a single org
     * @param $iOrgID
     */
    public function getOrg($iOrgID) { //set parameter iOrgID to get a single org
        $this->iOrgID = $iOrgID;
        $sql = "SELECT * FROM org WHERE iOrgID = ? AND iDeleted = 0";
        $row = $this->db->_fetch_array($sql, array($this->iOrgID));
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }
        //showme($row);
    }


    /**
     * @return int
     *
     */
    public function save() {
        if ($this->iOrgID > 0) {
            //UPDATE MODE
            $params = array(
                $this->vcOrgName,
                $this->vcAddress,
                $this->iZip,
                $this->vcCity,
                $this->vcEmail,
                $this->vcPhone,
                $this->vcFax,
                $this->vcCountry,
                $this->daCreated,
                $this->iOrgID,
            );

            $sql = "UPDATE org SET " .
                "vcOrgName = ?, " .
                "vcAddress = ?, " .
                "iZip = ?, " .
                "vcCity = ?, " .
                "vcEmail = ?, " .
                "vcPhone = ?, " .
                "vcFax = ?, " .
                "vcCountry = ?, " .
                "daCreated = ? " .
                "WHERE iOrgID = ? ";

            $this->db->_query($sql, $params);
            return $this->iOrgID;


        } else {
            //CREATE MODE
            $params = array(
                $this->vcOrgName,
                $this->vcAddress,
                $this->iZip,
                $this->vcCity,
                $this->vcEmail,
                $this->vcPhone,
                $this->vcFax,
                $this->vcCountry,
                time(),

            );

            $sql = "INSERT INTO org (" .
                "vcOrgName, " .
                "vcAddress, " .
                "iZip, " .
                "vcCity, " .
                "vcEmail, " .
                "vcPhone, " .
                "vcFax, " .
                "vcCountry, " .
                "daCreated) " .
                "VALUES(?,?,?,?,?,?,?,?,?)";
            showme($_POST);
            //exit();
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    public function delete() {
        $params = array($this->iOrgID);

        $sql = "UPDATE org set " .
            "iDeleted = 1 " .
            "WHERE iOrgID = ? ";
        $this->db->_query($sql, $params);

    }

}

