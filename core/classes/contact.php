<?php
/**
 * Created by PhpStorm.
 * User: tn116
 * Date: 14-09-2017
 * Time: 09:32
 */

class contact
{
    public $db;
    public $iMessageID;
    public $vcName;
    public $vcEmail;
    public $txContent;
    public $iDeleted;

    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iMessageID" => "ID",
            "vcName" => "contact Name",
            "vcEmail" => "Email",
            "txContent" => "Content"

        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iMessageID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "vcName" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcEmail" => array("textarea", FILTER_SANITIZE_STRING, TRUE, ""),
            "txContent" => array("textarea", FILTER_SANITIZE_STRING, TRUE, ""),


        );

        $this->arrValues = array();
    }

    /**
     * function to get list of categories
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM contact WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }


    /**
     * function to get a single contact
     * @param $iMessageID
     */
    public function getItem($iMessageID) { //set parameter iMessageID to get a single contact
        $this->iMessageID = $iMessageID;
        $sql = "SELECT * FROM contact WHERE iMessageID = ? AND iDeleted = 0";
        $row = $this->db->_fetch_array($sql, array($this->iMessageID));
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }
        //showme($row);
    }


    public function getAllCategories($Limit) {
        $sql = "SELECT * FROM contact WHERE iDeleted = 0 LIMIT " . $Limit;
        return $this->db->_fetch_array($sql);

    }


    /**
     * @return int
     *
     */
    public function save() {
        if ($this->iMessageID > 0) {
            //UPDATE MODE
            $params = array(
                $this->vcName,
                $this->vcEmail,
                $this->iMessageID,
            );

            $sql = "UPDATE contact SET " .
                "vcEmail = ?, " .
                "vcName = ?, " .
                "txContent = ? " .
                "WHERE iMessageID = ? ";

            $this->db->_query($sql, $params);
            return $this->iMessageID;


        } else {
            //CREATE MODE
            $params = array(
                $this->vcName,
                $this->vcEmail,
                $this->txContent,

            );

            $sql = "INSERT INTO contact (" .
                "vcName, " .
                "vcEmail, " .
                "txContent) " .
                "VALUES(?,?,?)";
            //exit();
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    public function delete() {
        $params = array($this->iMessageID);

        $sql = "UPDATE contact set " .
            "iDeleted = 1 " .
            "WHERE iMessageID = ? ";
        $this->db->_query($sql, $params);

    }

}

