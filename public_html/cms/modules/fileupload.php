<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

sysHeader();
?>


        <div class="container-fluid">
            <h1>Upload your files here</h1>
            <form action="../../upload.php" method="post" enctype="multipart/form-data">
                <input type="file" class="form-control" name="file">
                <button type="submit" class="btn btn-success" name="submit">Upload File</button>
            </form>
        </div>
<?php

sysFooter();
