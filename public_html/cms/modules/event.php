<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

$mode = setMode();

$strModuleName = "Event";

switch (strtoupper($mode)) {

    case "LIST";
        $strModuleMode = "Overview";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iEventID=-1')", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $event = new event();

        /* Array with fields and friendly names for list purposes*/
        $arrColumns = array(
            "opts" => "Options",
            "vcTitle" => "Title",
            "daStart" => "Start Date",
            "daStop" => "Stop Date",
            "iIsActive" => "Active",
        );

        /* Array for all event rows */
        $arrEvents = array();

        /* List orgs and set editing options */
        foreach ($event->getlist() as $key => $arrValues) {
            $arrValues["iIsActive"] = boolToIcon($arrValues["iIsActive"]);
            $arrValues["opts"] = getIcon("?mode=details&iEventID=" . $arrValues["iEventID"], "eye") .
                getIcon("?mode=edit&iEventID=" . $arrValues["iEventID"], "pencil") .
                getIcon("", "trash", "Slet Event", "remove(" . $arrValues["iEventID"] . ")");

            /* Add value row to arrUsers */
            $arrEvents[] = $arrValues;
        }

        /* Call list presenter object with columns (arrColumns) and rows (arrEvents) */
        $p = new listPresenter($arrColumns, $arrEvents);
        echo $p->presentlist();

        sysFooter();
        break;

    case "DETAILS";
        $iEventID = filter_input(INPUT_GET, "iEventID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Details";

        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=details", "Details", "btn-primary");
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iEventID=" . -1, "New", "btn-success");

        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        $event = new event();
        $event->getEvent($iEventID);


        $arrValues = get_object_vars($event);

        /*Converts date/stamp to readable date */
        $arrValues["iIsActive"] = boolToIcon($arrValues["iIsActive"]);
        $arrValues["daCreated"] = date2local($arrValues["daCreated"]);
        $arrValues["daStart"] = time2local($arrValues["daStart"]);
        $arrValues["daStop"] = time2local($arrValues["daStop"]);

        $presenter = new listPresenter($event->arrLabels, $arrValues);
        echo $presenter->presentdetails();


        sysFooter();
        break;

    case "EDIT";
        $iEventID = (int)filter_input(INPUT_GET, "iEventID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = "Edit";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "Details", "getUrl('?mode=details&iEventID=" . $iEventID . "'),");
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iEventID=-1')");
        $arrButtonPanel[] = getButton("button", "Overview", "getUrl('?mode=list')");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        /* Create class instance and set current org */
        $event = new event();

        /* Get org if state = update */
        if ($iEventID > 0) {
            $event->getevent($iEventID);
        }

        /* Get property values */
        $arrValues = get_object_vars($event);

        /* Get orgs as venues */
        $strSelect = "SELECT iOrgID, vcOrgName FROM org WHERE iDeleted = 0 ORDER BY vcOrgName";
        $arrOrgs = $db->_fetch_array($strSelect);
        /* Add a default value to the selectbox */
        array_unshift($arrOrgs, array("iOrgID" => 0, "vcOrgName" => "Vælg venue"));

        $arrValues["iVenueID"] = formpresenter::inputSelect("iVenueID", $arrOrgs, $event->iVenueID);

        /* Create presenter instance and set form */
        $form = new formpresenter($event->arrLabels, $event->arrFormElms, $arrValues);
        echo $form->presentForm();

        sysFooter();
        break;

    case "SAVE":
        $event = new event();

        /*
         * Loop form elements from org class & set org property if exists
         * Otherwise set default value from form elements
         * (Defined in org class)
         */

        foreach ($event->arrFormElms as $field => $arrTypes) {
            $event->$field = filter_input(INPUT_POST, $field, $arrTypes[1], getDefaultValue($arrTypes[3]));
        }

        $event->daStart = makeStamp("daStart");
        $event->daStop = makeStamp("daStop");

        /* Save method*/
        $iEventID = $event->save();
        header("Location: ?mode=details&iEventID=" . $iEventID);
        break;

    case "DELETE":
        $event = new event();
        $event->iEventID = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $event->delete($iEventID);
        header("Location: ?mode=list");
        break;


}

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/footer.php"; ?>
<script src="/public_html/assets/js/ajaxFunctions.js"></script>
