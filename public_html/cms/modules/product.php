<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

$mode = setMode();

$strModuleName = "product";

switch (strtoupper($mode)) {

    case "LIST";
        $strModuleMode = "Overview";

        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iProductID=-1')", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $product = new product();


        /* Array with fields and friendly names for list purposes*/
        $arrColumns = array(
            "opts" => "Options",
            "vcTitle" => "Title",
            "iIsActive" => "Active",
        );


        /* Array for all product rows */
        $arrproducts = array();

        /* List orgs and set editing options */
        foreach ($product->getlist() as $key => $arrValues) {
            $arrValues["iIsActive"] = boolToIcon($arrValues["iIsActive"]);
            $arrValues["opts"] = getIcon("?mode=details&iProductID=" . $arrValues["iProductID"], "eye") .
                getIcon("?mode=edit&iProductID=" . $arrValues["iProductID"], "pencil") .
                getIcon("", "trash", "Slet product", "remove(" . $arrValues["iProductID"] . ")");

            /* Add value row to arrproducts */
            $arrproducts[] = $arrValues;


        }

        /* Call list presenter object with columns (arrColumns) and rows (arrproducts) */
        $p = new listPresenter($arrColumns, $arrproducts);
        echo $p->presentlist();

        sysFooter();
        notify::pushMessage("it works", "deleted");
        break;

    case "DETAILS";
        $iProductID = filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Details";

        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iProductID=" . $iProductID, "Edit product", "btn-success");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=ingredients&iProductID=" . $iProductID, "Choose Ingredients", "btn-success");

        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        $product = new product();
        $product->getproduct($iProductID);


        $arrValues = get_object_vars($product);


        $presenter = new listPresenter($product->arrLabels, $arrValues);
        echo $presenter->presentdetails();


        sysFooter();
        break;

    case "EDIT";
        $iProductID = (int)filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = "Edit";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=details", "Details", "btn-primary");
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iProductID=" . -1, "New", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        /* Create class instance and set current org */
        $product = new product();

        /* Get org if state = update */
        if ($iProductID > 0) {
            $product->getproduct($iProductID);
        }

        /* Get property values */
        $arrValues = get_object_vars($product);

        /* Get orgs as venues */
        $strSelect = "SELECT iCatID, vcName FROM category WHERE iDeleted = 0 ORDER BY vcName";
        $arrCats = $db->_fetch_array($strSelect);
        /* Add a default value to the selectbox */
        array_unshift($arrCats, array("iCatID" => 0, "vcName" => "Choose Category"));

        $arrValues["iCatID"] = formpresenter::inputSelect("iCatID", $arrCats, $product->iCatID);


        /* Create presenter instance and set form */
        $form = new formpresenter($product->arrLabels, $product->arrFormElms, $arrValues);
        echo $form->presentForm();

        sysFooter();
        break;

    case "SAVE":
        $product = new product();


        foreach ($product->arrFormElms as $field => $arrTypes) {
            $product->$field = filter_input(INPUT_POST, $field, $arrTypes[1], getDefaultValue($arrTypes[3]));
        }


        /* Save method*/
        $iProductID = $product->save();


        header("Location: ?mode=details&iProductID=" . $iProductID);
        break;

    case "DELETE":
        $product = new product();
        $product->iProductID = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);

        $product->delete($iProductID);

        header("Location: ?mode=list");
        break;


    case "INGREDIENTS":
        $iProductID = filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Ingredients";
        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("eye", "?mode=details&iProductID=" . $iProductID, "See Details", "btn-primary");
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $arrSelected = array();
        $params = array($iProductID);

        $strSelect = "SELECT iIngredientsID, vcAmount FROM product_ingredients WHERE iProductID = ?";
        $rel = $db->_fetch_array($strSelect, $params);

        foreach ($rel as $value) {
            $arrSelected[] = $value["iIngredientsID"];
            $arrSelected[] = $value["vcAmount"];

        }


        $ingredient = new ingredients();
        $rows = $ingredient->getlist();


        $arrFormValues = array();
        $arrFormValues["iProductID"] = $iProductID;

        ?>

        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <form method="POST">
                        <input type='hidden' name='mode' value='SAVEINGREDIENTS'>
                        <?php


                        foreach ($rows as $key => $arrValues) {
                            $field = $arrValues["iIngredientsID"];
                            $arrFormValues[$field] = in_array($arrValues["iIngredientsID"], $arrSelected) ? 1 : 0;
                            $strIsChecked = $arrFormValues[$field] ? "checked=\"checked\"" : "";
                            ?>
                            <div class="col-md-12 form-group">
                                <label>
                                    <?php echo $arrValues["vcTitle"];
                                    ?>
                                    <input type="checkbox" name="groups[]"
                                           value="<?php echo $field; ?>" <?php echo $strIsChecked ?>>
                                    <input type="text" class="form-control"
                                           name="ingredients[<?php echo $arrValues["iIngredientsID"]; ?>]"
                                           placeholder="amount" value="">
                                </label>
                            </div>
                            <?php
                        } ?>
                        <button name='submit' class="btn btn-success" onclick="validate(this.form)">SAVE
                        </button>
                        <a href='?mode=list' class="btn btn-danger">CANCEL</a>
                    </form>
                </div>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="/assets/js/functions.js"></script>
        <script src="/cms/assets/js/validate.js"></script>
        <script src="/assets/js/ajaxFunctions.js"></script>

        <?php

        sysFooter();
        break;

    case "SAVEINGREDIENTS":
        $iProductID = filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);

        $params = array($iProductID);
        $strDelete = "DELETE FROM product_ingredients WHERE iProductID = ?";
        $db->_query($strDelete, $params);

        $args = array(
            "ingredients" => array(
                "filter" => FILTER_SANITIZE_STRING,
                "flags" => FILTER_REQUIRE_ARRAY
            ),
            "groups" => array(
                "filter" => FILTER_VALIDATE_INT,
                "flags" => FILTER_REQUIRE_ARRAY
            )
        );
        $arrInputVal = filter_input_array(INPUT_POST, $args);

        if (count($arrInputVal["groups"] && $arrInputVal["ingredients"])) {

            $arrGroups = array_values($arrInputVal["groups"]);
            $arrIng = array_values($arrInputVal["ingredients"]);

            $arrCombined = array_combine($arrGroups, $arrIng);
            //showme($arrCombined);

            foreach ($arrCombined as $key => $value) {
                //showme($value);
                $params = array($iProductID, $key, $value);
                echo $strInsert = "INSERT INTO product_ingredients(iProductID, iIngredientsID, vcAmount) VALUES(?,?,?)";
                $db->_query($strInsert, $params);
            }

        }
        header("Location: ?mode=details&iProductID=" . $iProductID);

        break;

}


require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/footer.php"; ?>
