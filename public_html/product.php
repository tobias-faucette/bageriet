<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
$mode = setMode();


switch (strtoupper($mode)) {

    case "LIST";
        require_once DOCROOT . "/assets/incl/header.php";
        $category = new category();
        $categories = $category->getAllCategories(5);

        $product = new product();
        $products = $product->getlist();

        $comment = new comment();
        $iProductID = $product->iProductID;

        ?>

        <!--PRODUCT SECTION-->
        <section class="margin-top-5percent">
            <div class="container product-section margin-bot-10percent">
                <div class="col-xs-12 text-center margin-bot-5percent">
                    <article>
                        <div class="" data="">
                            <h1 class="headline text-black">Vores elskede bagværk</h1>
                            <p class="">
                                Der er mange tilgængelige udgaver af Lorem Ipsum, men de fleste udgaver har gennemgået
                                forandringer, når nogen har <br> tilføjet humor eller tilfædige ord, som på ingen måde
                                ser ægte
                                ud
                            </p>
                        </div>
                    </article>
                </div>
                <div class="col-xs-12 col-sm-3 text-center">
                    <ul class="nav">
                        <?php $i = 0 ?>
                        <?php foreach ($categories as $key => $row): ?>
                            <li><a class="bread-cat <?php echo($i == 0 ? "cat-active" : "") ?>"
                                   data-cat="<?php echo $row["iCatID"] ?>"
                                   href="#"><?php echo mb_strtoupper($row["vcName"]) ?>
                                </a></li>
                            <?php $i++ ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <?php foreach ($products as $key => $row): ?>
                        <article data-parent="<?php echo $row["iCatID"] ?>">

                            <div class="col-xs-12 col-sm-4 text-center">
                                <img src="images/<?php echo $row["vcImage"] ?>" class="img-circle img-responsive">
                                &nbsp;<?php echo $comment->commentCount($row["iProductID"]) ?>

                                <i class="fa fa-comments-o"
                                   aria-hidden="true">
                                </i>
                                <h4 class="margin-bot-10percent text-black">
                                    <strong><?php echo $row["vcTitle"] ?></strong></h4>
                                <h4><?php $row["vcTitle"] ?></h4>
                                <p class="margin-bot-10percent"><?php if (strlen($row["txDesc"]) > 50) {
                                        $row["txDesc"] = substr($row["txDesc"], 0, 88) . "...";
                                    } else {
                                        $row["txDesc"] = $row["txDesc"] . "...";
                                    }
                                    echo $row["txDesc"] ?></p>
                                <?php echo $arrButtonPanel[] = getButtonLink("", "?mode=details&iProductID=" . $row["iProductID"], "Læs Mere", "product-btn"); ?>

                            </div>
                        </article>

                    <?php endforeach; ?>
                </div>
            </div>
        </section>
        <?php

        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";

        break;

    case "DETAILS";
        $iProductID = filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);

        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/header.php";

        $user = new user();
        $iUserID = $user->iUserID;
        $user->getUser($iUserID);

        $product = new product();
        $rows = $product->getProduct($iProductID);

        $comment = new comment();

        $num_comments = $db->_fetch_value("SELECT count(*) FROM comment WHERE iProductID = $iProductID");

        $ingredientRel = $product->getIngredientRel($iProductID);


        ?>


        <div class="container margin-top-5percent">
            <div class="col-xs-12 col-sm-8 ">
                <h4 class="text-black"><strong><?php echo mb_strtoupper($product->vcTitle) ?></strong></h4>
                <h5 class="margin-bot-10percent"><strong><?php echo mb_strtoupper($product->vcName) ?></strong></h5>
                <div>
                    <p>
                        <img align="left" src="images/<?php echo $product->vcImage ?>">
                        <?php echo $product->txDesc ?>
                    </p>
                </div>


            </div>
            <div id="ingredients" class="col-xs-12 col-sm-4">
                <div class="col-xs-12">
                    <button class="btn btn-default like-btn pull-right margin-bot-10percent"><strong>LIKE!</strong> <i
                                class="fa fa-heart-o" aria-hidden="true"></i></button>

                </div>
                <h4 class="text-black margin-top-10percent"><strong><?php echo "Ingredienser" ?></strong></h4>

                <?php foreach ($ingredientRel as $key): ?>
                    <p class="well"><?php echo $key["vcAmount"] . "." . " " . $key["vcTitle"] ?></p>
                <?php endforeach; ?>

            </div>


            <!--COMMENT SECTION-->
            <section>
                <div class="col-xs-12 margin-top-5percent">
                    <div class="well">
                        <h4 class="text-black">Kommentar<span class="pull-right">
                                <span class="comment-count"> <?php echo $num_comments ?>
                                </span>
                                <i class=" fa fa-comments-o" aria-hidden="true"></i>
                        </span></h4>

                    </div>
                </div>
                <?php if ($auth->checkSession()) { ?>
                    <div class="col-xs-12 margin-bot10">

                        <div class="col-xs-12 comment padding-zero">
                            <form method="POST" id="comment_form">
                                <input type="hidden" id="iProductID" name="iProductID"
                                       value="<?php echo $iProductID ?>">
                                <div class="col-xs-12 col-sm-6">
                                    <input type="hidden" id="vcName" class="form-control" name="vcName"
                                           value="<?php echo $auth->user->vcFirstName . "&nbsp;" . $auth->user->vcLastName ?>">
                                </div>
                                <div class="col-xs-12 padding-zero">
                                    <div class="col-xs-12 col-sm-10 padding-zero">
                                        <span class="textarea-icon"><i class="fa fa-pencil"
                                                                       aria-hidden="true"></i></span>
                                        <textarea row="1" data-min-rows="1" id="txContent" name="txContent"
                                                  class="form-control" placeholder="Fortæl os hvad du synes...."
                                                  required data-required="1"
                                                  data-validate="textarea"></textarea>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 padding-zero">
                                        <button type="button" class="btn-comment" id="submit_comment"
                                                onclick="validate(this.form)">Indsæt
                                        </button>
                                    </div>


                                </div>
                            </form>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="col-xs-12 margin-bot15">
                        <h3>Du skal logge ind for at skrive kommentare</h3>
                        <hr>
                    </div>
                    <?php
                }
                ?>
                <?php
                /**
                 * comment section
                 */


                /**
                 * Pagination
                 */

                $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT,
                    array("options" => array("default" => 1)));
                $limit = 3;
                $row = $comment->listComments($iProductID, $limit);

                $start = ($page - 1) * $limit;

                $params = array($start, $limit);
                $sql = "SELECT * FROM comment WHERE iProductID = $iProductID LIMIT ?, ?";
                $rows = $db->_fetch_array($sql, $params);

                $num_records = $db->_fetch_value("SELECT COUNT(*) FROM comment WHERE iProductID = " . $iProductID);
                $num_pages = ceil($num_records / $limit);


                //                if ($row > 0) {
                foreach ($rows as $key): ?>
                    <div class="col-xs-12 comment-article margin-bot5">
                        <article>
                            <div class="col-xs-12 well">
                                <div class="col-xs-12 col-sm-2">
                                    <img class="img-responsive img-circle" src="https://www.placecage.com/100/100"

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-10">
                                <h4>
                                    <strong><?php echo $key["vcName"] ?></strong>
                                </h4>
                                <?php echo date('j F \k\l H:i Y', $key["daCreated"]) ?>
                                <p><?php echo $key["txContent"] ?></p>
                            </div>
                        </article>

                    </div>
                <?php endforeach;
                //                }
                ?>

            </section>
            <div class="col-xs-12">
                <section class="row section-space-small">
                    <ul class="pagination no-border-radius">

                        <?php $prevPage = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT); ?>
                        <li>
                            <a href="?mode=Details&iProductID=<?php echo $iProductID ?>&page=<?php ($prevPage > 1 ? $prevPage-- : $prevPage);
                            echo $prevPage; ?>">&laquo;</a></li>

                        <?php
                        for ($i = 1; $i <= $num_pages; $i++) {
                            echo "<li><a class='no-border-radius page_numbers' href=\"?mode=Details&iProductID=" . $iProductID . "&page=" . $i . "\">" . $i . "</a></li>&nbsp;\n";
                        }
                        $nextPage = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT);
                        ?>
                        <li>
                            <a href="?mode=Details&iProductID=<?php echo $iProductID ?>&page=<?php ($nextPage < $num_pages ? $nextPage++ : $nextPage);
                            echo $nextPage; ?>">&raquo;</a></li>


                    </ul>
                </section>
            </div>


        </div>


        <?php

        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";

        break;

}


