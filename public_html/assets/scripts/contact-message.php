<?php require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";


$vcName = filter_input(INPUT_POST, "vcName", FILTER_SANITIZE_STRING);
$vcEmail = filter_input(INPUT_POST, "vcEmail", FILTER_SANITIZE_STRING);
$txContent = filter_input(INPUT_POST, "txContent", FILTER_SANITIZE_STRING);


$params = array(
    $vcName,
    $vcEmail,
    $txContent
);


$sql = "INSERT into contact (" .
    "vcName, " .
    "vcEmail, " .
    "txContent) " .
    "VALUES(?,?,?)";


$db->_query($sql, $params);


header('location: /contact.php');