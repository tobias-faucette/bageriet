<?php
/**
 * Created by PhpStorm.
 * User: tn116
 * Date: 04-09-2017
 * Time: 10:33
 * Comment PHP script
 * Author Tobias N.
 *
 */
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";


$iProductID = filter_input(INPUT_POST, "iProductID", FILTER_SANITIZE_NUMBER_INT);
$vcName = filter_input(INPUT_POST, "vcName", FILTER_SANITIZE_STRING);
$txContent = filter_input(INPUT_POST, "txContent", FILTER_SANITIZE_STRING);

if (isset($_POST['done'])) {

    //Saves data to database


    $params = array(
        $iProductID,
        $vcName,
        $txContent,
        time()
    );


    $sql = "INSERT into comment (" .
        "iProductID, " .
        "vcName, " .
        "txContent, " .
        "daCreated) " .
        "VALUES(?,?,?,?)";


    $db->_query($sql, $params);


}