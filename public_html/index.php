<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
$mode = setMode();


switch (strtoupper($mode)) {

    case "LIST";



        $news = new news();
        $rows = $news->getAllNews(3);

        $product = new product();
        $products = $product->getAllProduct(8);


        $comment = new comment();
        $iProductID = $product->iProductID;

        $slider = new slider();
        $slider_image = $slider->getlist();

        $slideCounter = $slider->sliderCounter();
        $i = 0;



        ?>
        <div class="transparent-bg"><?php require_once DOCROOT . "/assets/incl/header.php"; ?></div>
        <section class="margin-bot-5percent">
            <div class="col-xs-12 hidden-xs padding-zero">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php
                        for ($i = 0; $i < $slideCounter["COUNT(*)"]; $i++) { ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $i ?>" class="<?php echo ($i === 0 ? "active" : "") ?>"></li>
                        <?php
                        }
                        ?>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">


                        <?php foreach ($slider_image as $slider): ?>

                            <?php if($slider["iIsActive"] == 1) { ?>
                        <div class="item active">
                            <img src="images/<?php echo $slider["vcImage"] ?>" alt="<?php echo $slider["vcTag"] ?>">
                            <div class="carousel-caption">
                                <h1><?php echo $slider["vcCaption"] ?></h1>
                            </div>
                        </div>

                            <?php } else { ?>
                        <div class="item">
                            <img src="images/<?php echo $slider["vcImage"] ?>" alt="<?php echo $slider["vcTag"] ?>">
                            <div class="carousel-caption">
                                <h1><?php echo $slider["vcCaption"] ?></h1>
                            </div>
                        </div>
                        <?php } ?>
                        <?php endforeach; ?>


                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </section>
        <!-- SERVICE SECTION-->
        <section>
            <div class="container margin-bot-5percent">
                <div class="col-xs-12 text-center margin-bot-5percent">
                    <h1 class="headline text-black">Vi skaber lækkert! Brød</h1>
                    <p>Der er mange tilgængelige udgaver af Lorem Ipsum, men de fleste udgaver har gennemgået
                        forandringer, når nogen har <br> tilføjet humor eller tilfædige ord, som på ingen måde ser ægte
                        ud</p>
                </div>

                <div class="col-xs-12">
                    <?php foreach ($rows as $key => $row): ?>
                        <article>
                            <div class="col-xs-12 col-sm-4 text-center">
                                <div class="image text-center">

                                    <img src="images/<?php echo $row["vcImage"] ?>" class="img-circle img-responsive">
                                    <img>
                                </div>
                                <h4><?php echo $row["vcTitle"] ?></h4>
                                <p><?php if (strlen($row["txDesc"]) > 50) {
                                        $row["txDesc"] = substr($row["txDesc"], 0, 117) . "...";
                                    } else {
                                        $row["txDesc"] = $row["txDesc"] . "...";
                                    }
                                    echo $row["txDesc"] ?></p>
                            </div>
                        </article>
                    <?php endforeach; ?>

                </div>
            </div>

        </section>


        <!--NEWSLETTER SECTION-->
        <section class="margin-bot-5percent">

            <div class="container-fluid newsletter padding-zero">

                <div class="container">
                    <div class="col-xs-12  margin-bot15">
                        <h1 class="headline">Tilmeld dig vores nyhedsbrev</h1>
                        <p>Ved at tilmelde dig vores nyhedsbrev er du altid sikret af få de nyeste opdateringer omkring vores opskrifter</p>
                        <form method="POST" action="assets/scripts/newsletter.php">
                            <div class="col-xs-10 padding-zero">
                                <div class="input-group margin-bottom-sm">
                                    <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                                    <input class="form-control" type="email" data-required="1"
                                           data-validate="validemail" id="vcEmail" required name="vcEmail" value=''
                                           placeholder="Indtast din email...">
                                </div>
                            </div>
                            <div class="col-xs-2 padding-zero">
                                <button type="button" class="btn btn-newsletter" onclick="validate(this.form)">
                                    <?php echo strtoupper("tilmeld") ?>
                                </button>

                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </section>


        <!--PRODUCT SECTION-->
        <section>
            <div class="container product-section margin-bot-10percent">
                <div class="col-xs-12 text-center margin-bot-5percent">
                    <article>
                        <h1 class="headline text-black">Nyeste bagværk</h1>
                        <p>
                            Der er mange tilgængelige udgaver af Lorem Ipsum, men de fleste udgaver har gennemgået
                            forandringer, når nogen har <br> tilføjet humor eller tilfædige ord, som på ingen måde ser
                            ægte ud
                        </p>
                    </article>
                </div>
                <div class="col-xs-12">
                    <?php foreach ($products as $key => $row): ?>
                        <div class="col-xs-12 col-sm-3 text-center">
                            <article>
                                <img src="images/<?php echo $row["vcImage"] ?>" class="img-circle img-responsive">
                                <?php echo $comment->commentCount($row["iProductID"]) ?>
                                <i class="fa fa-comments-o"
                                   aria-hidden="true">&nbsp;
                                </i>
                                <h4 class="margin-bot-10percent text-black">
                                    <strong><?php echo $row["vcTitle"] ?></strong></h4>
                                <h4><?php $row["vcTitle"] ?></h4>
                                <p class="margin-bot-10percent"><?php if (strlen($row["txDesc"]) > 50) {
                                        $row["txDesc"] = substr($row["txDesc"], 0, 88) . "...";
                                    } else {
                                        $row["txDesc"] = $row["txDesc"] . "...";
                                    }
                                    echo $row["txDesc"] ?></p>
                                <?php echo $arrButtonPanel[] = getButtonLink("", "product.php?mode=details&iProductID=" . $row["iProductID"], "Læs Mere", "product-btn"); ?>

                            </article>
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>
        </section>
        <?php

        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";

        break;

}


