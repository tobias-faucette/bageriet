<?php
/**
 * Created by PhpStorm.
 * User: tn116
 * Date: 25-09-2017
 * Time: 13:01
 */

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/header.php";

$org = new org();
$org->getOrg(7);
?>

    <div class="container margin-bot-10percent">
        <div class="col-xs-12  text-center margin-bot-5percent">
            <h1 class="headline text-black">Kontakt os</h1>
            <p>Der er mange tilgængelige udgaver af Lorem Ipsum, men de fleste udgaver har gennemgået forandringer, når
                nogen har <br> tilføjet humor eller tilfædige ord, som på ingen måde ser ægte ud</p>
        </div>
        <div class="col-xs-12 contact margin-bot-10percent">
            <div class="col-xs-12 col-sm-6">
                <form method="POST" id="contact-form" action="assets/scripts/contact-message.php">
                    <div class="col-xs-12">
                        <input type="text" id="vcName" class="form-control" placeholder="Dit navn...." required
                               name="vcName" data-required="1"
                               data-validate="validText"
                               value=''>
                    </div>
                    <div class="col-xs-12">
                        <input class='form-control' type='email' name='vcEmail' id='vcEmail' data-required="1"
                               data-validate="validemail" value='' placeholder="Din e-mail...">
                    </div>
                    <div class="col-xs-12">
                        <textarea rows="10" cols="50" id="txContent" name="txContent" class="form-control"
                                  placeholder="Din besked..." data-required="1"
                                  data-validate="textarea"
                                  value=''></textarea>
                    </div>
                    <div class="col-xs-4 margin-bot15 pull-right">
                        <button type="button" class="contact-btn form-control" onclick="validate(this.form)">Send
                        </button>

                    </div>
                </form>
            </div>
            <div class="col-xs-12 col-sm-6">
                <ul>
                    <li><strong>Adresse:</strong> <?php echo $org->vcAddress . " " . $org->iZip . " " . $org->vcCity ?>
                    </li>
                    <li><strong>telefon:</strong> <?php echo $org->vcPhone ?></li>
                </ul>

                <iframe class=""
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2170.215714792399!2d9.965507316323198!3d57.04784708091949!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x464932b69856edb3%3A0x565e91c76e25f34b!2s%C3%98ster+Uttrup+Vej+1%2C+9000+Aalborg!5e0!3m2!1sen!2sdk!4v1506368955435"
                        width="400" height="280" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>


<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";
