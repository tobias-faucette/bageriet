<?php
if (isset($_POST["submit"])) {
    $file = $_FILES['file'];


    $filename = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $fileSize = $_FILES['file']['size'];
    $fileError = $_FILES['file']['error'];
    $fileType = $_FILES['file']['type'];


    $fileExt = explode('.', $filename);
    $fileActualExt = strtolower(end($fileExt));

    //Allowed file types
    $allowed = array('jpg', 'jpeg', 'png', 'gif', 'pdf');


    //
    if (in_array($fileActualExt, $allowed)) {
        //If there are no errors, proceed with upload
        if ($fileError === 0) {
            //sets filesize
            if ($fileSize < 50000000) {
                //Created unique ID to prevent same file names and overwrites.
                $fileNameNew = uniqid('', 'TRUE') . "." . $fileActualExt;

                //Destination for files to be uploaded to.
                $fileDestination = 'images/' . $fileNameNew;
                move_uploaded_file($fileTmpName, $fileDestination);
            } else {
                //Error msg if file is too large to be uploaded
                echo "Your file is too large";

            }
        } else {
            //Error msg if file is unable to upload and $fileError is higher than 0
            echo "There was an error uploading your file!";

        }
    } else {
        //Error msg if file is not allowed type
        echo "You cannot upload files of this type";
    }
}

header("Location:cms/modules/fileupload.php");